package main

import (
	"cloud-assignment2-currencyhook/functionality"
	"time"
)

func main() {

	oneOffSens := 1

	hour, minute, _ := time.Now().UTC().Clock()

	if hour == 17 || hour == 16 && minute <= oneOffSens || minute >= 60 - oneOffSens {
		db := functionality.GetADB()
		db.Init()
		db.DailyCheckHooks()
		functionality.DailyRoutine()
	}
}
