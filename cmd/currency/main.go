// Author: Aksel Hjerpbakk
// Studentnr: 997816

package main

import (
	"cloud-assignment2-currencyhook/functionality"
	"os"
	"net/http"
)

func main() {
	port := os.Getenv("PORT")
	http.HandleFunc("/", functionality.URLHandler)
	http.ListenAndServe(":"+port, nil)
}
