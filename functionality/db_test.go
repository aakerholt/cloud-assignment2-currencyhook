package functionality

import (
	"encoding/json"
	"gopkg.in/mgo.v2"
	"testing"
)

func TestMongoDBInfo_Add(t *testing.T) {

	testHook := CurrencyLoad{}

	testBody := []byte(`{"webhookURL":"deleteMe","baseCurrency":"EUR","targetCurrency":"NOK","minTriggerValue":8,"maxTriggerValue":10}`)

	json.Unmarshal(testBody, &testHook)

	db := GetADB()
	db.Init()

	session, err := mgo.Dial(db.MongoURL)
	if err != nil {
		t.Fatal(err)
	}
	defer session.Close()

	err = session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Insert(testHook)
	if err != nil {
		t.Error("Failed to insert test data")
	}

	testHookRes, got := db.Get(testHook.WebHookURL)
	if got == false {
		t.Error("Could not retrieve added data")
	}
	if testHookRes.WebHookURL != testHook.WebHookURL || testHookRes.MinTriggerValue != testHook.MinTriggerValue {
		t.Error("Failed to add correct data")
	}
	db.DeleteByURL("deleteMe")
}

func TestMongoDBInfo_Get(t *testing.T) {

	testHook := CurrencyLoad{}

	testBody := []byte(`{"webhookURL":"keke","baseCurrency":"EUR","targetCurrency":"NOK","minTriggerValue":8,"maxTriggerValue":10}`)

	json.Unmarshal(testBody, &testHook)

	db := GetADB()
	db.Init()

	testHookRes, got := db.Get(testHook.WebHookURL)
	if got == false {
		t.Error("Could not retrieve added data")
	}
	if testHookRes.WebHookURL != testHook.WebHookURL || testHookRes.MinTriggerValue != testHook.MinTriggerValue {
		t.Error("Failed to add correct data")
	}

}
