package functionality

// TODO: replace prints with error handling

import (
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"strings"
	"time"
)

// TODO: report that base is not implemented

func RootHandler(w http.ResponseWriter, r *http.Request) {
	// TODO: Move data into a rawFixer to check if we find a rate.

	if r.Method == "POST" {

		//r.Header.Set("Content-Type", "application/json")

		myClient := http.Client{
			Timeout: time.Second * 2,
		}

		newCurrencyLoad, err := getCurrencyLoad(r)
		if err != nil {
			http.Error(w, "Could not retrieve post data in form currencyLoad", http.StatusInternalServerError)
			return
		}

		if newCurrencyLoad.BaseCurrency != "EUR" {
			http.Error(w, "Use EUR as base", http.StatusNotImplemented)
			return
		}

		validString, err := regexp.MatchString("^[A-Z]{3}$", newCurrencyLoad.TargetCurrency)
		if err != nil {
			http.Error(w, "Something went wrong under regexp match", http.StatusInternalServerError)
			return
		} else if validString == false {
			http.Error(w, "Invalid target currency!", http.StatusConflict)
			return
		}

		db := GetADB()
		db.Init()

		// create a fixer url
		fixerURL := "http://api.fixer.io/latest?base=EUR"

		fixerBody, fixerError := GetBody(fixerURL, &myClient)
		if fixerError != nil {
			http.Error(w, "Could not get body from fixer", http.StatusTooManyRequests)
			return
		}

		fixerString := string(fixerBody[:])

		if !(strings.Contains(fixerString, newCurrencyLoad.TargetCurrency)) {
			http.Error(w, "Target currency is not valid!", http.StatusConflict)
			return
		}

		// 3. Save webhook to mongoDB
		newCurrencyLoad.BaseCurrency = "EUR"

		err = db.Add(newCurrencyLoad)
		if err != nil {
			http.Error(w, "Failed to add", http.StatusInternalServerError)
			return
		}

		addedHook, found := db.Get(newCurrencyLoad.WebHookURL)
		if found == false {
			http.Error(w, "Could not find ID!", http.StatusNotFound)
			return
		}

		frontID := strings.Split(addedHook.Id.String(), "\"")

		fmt.Fprintf(w, "ID: %s", frontID[1])

	} else {
		http.Error(w, "Please post json to create webhook", http.StatusBadRequest)
	}
}

func IdHandler(w http.ResponseWriter, r *http.Request) {

	db := GetADB()
	db.Init()

	hookURL := r.URL.Path

	idString := strings.Split(hookURL, "/")

	if r.Method == "GET" {
		webHook, gotIt := db.GetByID(idString[1])
		if gotIt == false {
			http.Error(w, "Could not find web hook", http.StatusNotFound)
			return
		}
		w.WriteHeader(http.StatusOK)
		http.Header.Add(w.Header(), "content-type", "application/json")
		json.Marshal(&webHook)
		json.NewEncoder(w).Encode(webHook)

	} else if r.Method == "DELETE" {
		_, got := db.Get(idString[1])
		if got == true {

			wasDeleted := db.Delete(idString[1])
			if wasDeleted == false {
				http.Error(w, "Could not find web hook", http.StatusNotFound)
				return
			}
			w.WriteHeader(http.StatusOK)
		} else {
			w.WriteHeader(http.StatusNotFound)
		}
	} else {
		w.WriteHeader(http.StatusConflict)
	}
}

func LatestHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		db := GetADB()
		db.Init()

		postRates := Rates{}

		err := json.NewDecoder(r.Body).Decode(&postRates)
		if err != nil {
			http.Error(w, "Could not retrieve data from post", http.StatusConflict)
			return
		}

		aDate := time.Now().Local()
		stringTodayDate := aDate.Format("2006-01-02")

		rawFixer := RawFixer{}

		DayRate, gotLocal := db.GetLocalFixer(stringTodayDate)
		if gotLocal == false {

			aDate = aDate.AddDate(0, 0, -1)
			stringTodayDate = aDate.Format("2006-01-02")

			DayRate, gotLocal = db.GetLocalFixer(stringTodayDate)
			if gotLocal == false {
				http.Error(w, "Missing data about today", http.StatusInternalServerError)
				return
			}
		}

		dayRateBody, err := json.Marshal(DayRate)
		if err != nil {
			http.Error(w, "Failed to convert internal data 1", http.StatusInternalServerError)
			return
		}
		err = json.Unmarshal(dayRateBody, &rawFixer)
		if err != nil {
			http.Error(w, "Failed to convert internal data 2", http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		fmt.Fprintln(w, rawFixer.LocalRate[postRates.TargetCurrency])

	} else {
		http.Error(w, "Wrong method incoming", http.StatusConflict)
		return
	}
}

func AverageHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST" {

		const AverageOf = 3

		postRates := Rates{}

		err := json.NewDecoder(r.Body).Decode(&postRates)
		if err != nil {
			http.Error(w, "Could not retrieve data from post", http.StatusConflict)
		}

		db := GetADB()
		db.Init()

		// Get today's date
		aDate := time.Now().Local()
		stringTodayDate := aDate.Format("2006-01-02")

		rawFixer := RawFixer{}

		var rateSum float64

		for i := 0; i < AverageOf; i++ {
			aDate = time.Now().Local().AddDate(0, 0, -i)
			stringTodayDate = aDate.Format("2006-01-02")

			DayRate, gotLocal := db.GetLocalFixer(stringTodayDate)
			if gotLocal == false {
				http.Error(w, "Missing data about one of the days", http.StatusInternalServerError)
				return
			}

			dayRateBody, err := json.Marshal(DayRate)
			if err != nil {
				http.Error(w, "Failed to convert internal data", http.StatusInternalServerError)
				return
			}
			err = json.Unmarshal(dayRateBody, &rawFixer)
			if err != nil {
				http.Error(w, "Failed to convert internal data", http.StatusInternalServerError)
				return
			}
			println(rawFixer.Date)

			rateSum += rawFixer.LocalRate[postRates.TargetCurrency]
		}

		averageRate := rateSum / AverageOf
		w.WriteHeader(http.StatusOK)
		fmt.Fprintln(w, averageRate)
	} else {
		http.Error(w, "Wrong method incoming", http.StatusConflict)
		return
	}

}

func EvalHandler(w http.ResponseWriter, r *http.Request) {
	db := GetADB()
	db.Init()
	db.EvalDailyCheckHooks()
}

func URLHandler(w http.ResponseWriter, r *http.Request) {
	// TODO: Find out how to seperate
	URLCheck := r.URL.Path
	URLSplit := strings.Split(URLCheck, "/")

	if strings.ToLower(URLSplit[1]) == "" {
		RootHandler(w, r)
		return
	} else if strings.ToLower(URLSplit[1]) == "latest" {
		LatestHandler(w, r)
	} else if strings.ToLower(URLSplit[1]) == "average" {
		AverageHandler(w, r)
	} else if strings.ToLower(URLSplit[1]) == "evaluationtrigger" {
		EvalHandler(w, r)
	} else {
		IdHandler(w, r)
	}

}
