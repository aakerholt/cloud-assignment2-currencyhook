package functionality

import (
	"encoding/json"
	"fmt"
	"gopkg.in/mgo.v2"
	"io/ioutil"
	"net/http"
)

func GetADB() *MongoDBInfo {
	db := MongoDBInfo{
		"mongodb://Avokado:Verify123@ds243805.mlab.com:43805/currencydb",
		"currencydb",
		"hookCollection",
		"ratesCollection",
	}

	_, err := mgo.Dial(db.MongoURL)
	if err != nil {
		panic(err)
	}

	return &db
}

// GetBody ...
// @Return: Body and error/nil
func GetBody(url string, myClient *http.Client) ([]byte, error) {

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		body := []byte(nil)
		return body, err
	}

	// Apply header to request
	req.Header.Set("User-Agent", "AkselHj")

	// Try to execute request
	res, doError := myClient.Do(req)
	if doError != nil {
		body := []byte(nil)
		return body, doError
	}

	defer res.Body.Close()

	body, readError := ioutil.ReadAll(res.Body)

	if readError != nil {
		return body, readError
	}

	return body, nil
}

func getCurrencyLoad(r *http.Request) (CurrencyLoad, error) {
	currencyLoad := CurrencyLoad{}

	err := json.NewDecoder(r.Body).Decode(&currencyLoad)
	if err != nil {
		return currencyLoad, err
	}

	defer r.Body.Close()
	return currencyLoad, err
}

func GetHookContent(hook CurrencyLoad, rawFixer RawFixer) string {

	a := fmt.Sprint(rawFixer.LocalRate[hook.TargetCurrency])
	b := fmt.Sprint(hook.MinTriggerValue)
	c := fmt.Sprint(hook.MaxTriggerValue)

	hookContent := "baseCurrency: " + hook.BaseCurrency + "\ntargetCurrency: " + hook.TargetCurrency +
		"\ncurrentRate: " + a +
		"\nminTriggerValue: " + b + "\nmaxTriggerValue: " + c

	return hookContent
}

func GetRates(myClient *http.Client, base string) (interface{}, error) {

	var jsonFixer interface{}

	fixerURL := "http://api.fixer.io/latest?base=" + base

	fixerBody, err := GetBody(fixerURL, myClient)
	if err != nil {
		return jsonFixer, err
	}

	err = json.Unmarshal(fixerBody, &jsonFixer)
	if err != nil {
		return jsonFixer, err
	}
	return jsonFixer, err
}
