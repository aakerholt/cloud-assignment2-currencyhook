package functionality

import (
	"encoding/json"
	"net/http"
	"strings"
	"testing"
	"time"
)

func TestGetBody(t *testing.T) {

	myClient := http.Client{
		Timeout: time.Second * 2,
	}

	testURL := "http://api.fixer.io/2017-04-09?base=EUR"

	testBody := []byte(`{"base":"EUR","date":"2017-04-07","rates":{"AUD":1.4123,"BGN":1.9558,"BRL":3.3349,"CAD":1.4256,
	"CHF":1.0695,"CNY":7.3318,"CZK":26.563,"DKK":7.4363,"GBP":0.85573,"HKD":8.2596,"HRK":7.449,
	"HUF":310.36,"IDR":14161.0,"ILS":3.8784,"INR":68.378,"JPY":117.64,"KRW":1206.4,"MXN":19.947,
	"MYR":4.7144,"NOK":9.1583,"NZD":1.5249,"PHP":53.019,"PLN":4.2249,"RON":4.5178,
	"RUB":60.492,"SEK":9.5963,"SGD":1.4902,"THB":36.79,"TRY":3.9726,"USD":1.063,"ZAR":14.684}}`)

	getBodyBody, err := GetBody(testURL, &myClient)
	if err != nil {
		t.Error(err)
	}

	testJson := RawFixer{}
	getBodyJson := RawFixer{}

	json.Unmarshal(testBody, &testJson)
	json.Unmarshal(getBodyBody, &getBodyJson)

	if testJson.Date != getBodyJson.Date || testJson.LocalRate["NOK"] != getBodyJson.LocalRate["NOK"] {
		t.Error()
	}

}

func TestGetADB(t *testing.T) {
	db := GetADB()
	s, got := db.Get("keke")
	if got != true {
		t.Error("DB can't retrieve data!")
	}

	testHook := CurrencyLoad{}

	testBody := []byte(`{"webhookURL":"keke","baseCurrency":"EUR","targetCurrency":"NOK","minTriggerValue":8,"maxTriggerValue":10}`)

	json.Unmarshal(testBody, &testHook)

	if s.WebHookURL != testHook.WebHookURL || s.TargetCurrency != testHook.TargetCurrency {
		t.Error("DB is not operating properly")
	}
}

func TestGetHookContent(t *testing.T) {

	rawBody := []byte(`{"base":"EUR","date":"2017-04-07","rates":{"AUD":1.4123,"BGN":1.9558,"BRL":3.3349,"CAD":1.4256,
	"CHF":1.0695,"CNY":7.3318,"CZK":26.563,"DKK":7.4363,"GBP":0.85573,"HKD":8.2596,"HRK":7.449,
	"HUF":310.36,"IDR":14161.0,"ILS":3.8784,"INR":68.378,"JPY":117.64,"KRW":1206.4,"MXN":19.947,
	"MYR":4.7144,"NOK":9.1583,"NZD":1.5249,"PHP":53.019,"PLN":4.2249,"RON":4.5178,
	"RUB":60.492,"SEK":9.5963,"SGD":1.4902,"THB":36.79,"TRY":3.9726,"USD":1.063,"ZAR":14.684}}`)

	testBody := []byte(`{"webhookURL":"deleteMe","baseCurrency":"EUR",
	"targetCurrency":"NOK","minTriggerValue":8,"maxTriggerValue":10}`)

	testJson := CurrencyLoad{}
	rawJson := RawFixer{}

	json.Unmarshal(rawBody, &rawJson)
	json.Unmarshal(testBody, &testJson)

	result := GetHookContent(testJson, rawJson)

	wanted := "baseCurrency: EUR\ntargetCurrency: NOK\ncurrentRate: 9.1583\nminTriggerValue: 8\nmaxTriggerValue: 10"

	simularity := strings.Compare(result, wanted)

	if simularity != 0 {
		t.Error("Did not retrieve corrrect data")
	}
}
