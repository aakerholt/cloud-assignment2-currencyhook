package functionality

import "testing"

func TestWebHookEntry(t *testing.T) {
	did := WebHookEntry("Hello world!",
		"https://discordapp.com/api/webhooks/375013560788451331/"+
			"4NO43EJrdT0pg-BL_RgPXgOuHYpiBvxFMljSxY6BwJapDhzO0wsADvPkcpEarD-LP5kB")
	if did != true {
		t.Error("failed to communicate with hook")
	}
	did = WebHookEntry("Goodbye world!",
		"Bip bop")
	if did == true {
		t.Error("Bool is wrong")
	}
}
