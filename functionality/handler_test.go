package functionality

// Sources:
// https://elithrar.github.io/article/testing-http-handlers-go/

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
)

func TestRootHandler(t *testing.T) {

	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(RootHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}

	testBody := []byte(`{"webhookURL":"deleteMe","baseCurrency":"EUR","targetCurrency":"NOK","minTriggerValue":8,"maxTriggerValue":10}`)
	testIo := bytes.NewReader(testBody)

	req, err = http.NewRequest("POST", "/", testIo)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(RootHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if !strings.Contains(rr.Body.String(), "ID") {
		t.Error("request body did not contain expected: got %s want %s",
			rr.Body.String(), "ID")
	}

	db := GetADB()
	db.DeleteByURL("deleteMe")
	_, got := db.Get("deleteMe")
	if got == true {
		t.Error("Failed to remove test object!")
	}

}

func TestIdHandler(t *testing.T) {
	req, err := http.NewRequest("POST", "/59fb3890a35109d282bd046f", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(IdHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusConflict {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusConflict)
	}

	req, err = http.NewRequest("DELETE", "/djs8675686sdhadjlkhashj", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(IdHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNotFound {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}

	req, err = http.NewRequest("GET", "/59fb3890a35109d282bd046f", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(IdHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if !strings.Contains(rr.Body.String(), `"webhookURL":"keke","baseCurrency":"EUR","targetCurrency":"NOK","minTriggerValue":8,"maxTriggerValue":10`) {
		t.Error("request body did not contain expected: got %s want %s",
			rr.Body.String(), `"webhookURL":"keke","baseCurrency":"EUR","targetCurrency":"NOK","minTriggerValue":8,"maxTriggerValue":10`)
	}
}

func TestLatestHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/latest", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(LatestHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusConflict {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusConflict)
	}

	req, err = http.NewRequest("DELETE", "/latest", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(LatestHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusConflict {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusConflict)
	}

	testBody := []byte(`{"baseCurrency":"EUR","targetCurrency":"NOK"}`)
	testIo := bytes.NewReader(testBody)

	req, err = http.NewRequest("POST", "/latest", testIo)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(LatestHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

func TestAverageHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/average", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(AverageHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusConflict {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusConflict)
	}

	req, err = http.NewRequest("DELETE", "/average", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(AverageHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusConflict {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusConflict)
	}

	testBody := []byte(`{"baseCurrency":"EUR","targetCurrency":"NOK"}`)
	testIo := bytes.NewReader(testBody)

	req, err = http.NewRequest("POST", "/latest", testIo)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(LatestHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	rBody := strings.TrimSpace(rr.Body.String())

	bodyNumber, err := strconv.ParseFloat(rBody, 64)
	if err != nil {
		t.Error(err)
	}

	if bodyNumber > 15 || bodyNumber < 4 {
		t.Error("Most likely wrong calculation when average is %v", rBody)
	}

}

func TestURLHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/average", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(URLHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusConflict {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusConflict)
	}

	req, err = http.NewRequest("DELETE", "/average", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(URLHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusConflict {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusConflict)
	}

	testBody := []byte(`{"baseCurrency":"EUR","targetCurrency":"NOK"}`)
	testIo := bytes.NewReader(testBody)

	req, err = http.NewRequest("POST", "/latest", testIo)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(URLHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	rBody := strings.TrimSpace(rr.Body.String())

	bodyNumber, err := strconv.ParseFloat(rBody, 64)
	if err != nil {
		t.Error(err)
	}

	if bodyNumber > 15 || bodyNumber < 4 {
		t.Error("Most likely wrong calculation when average is %v", rBody)
	}

	req, err = http.NewRequest("GET", "/latest", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(URLHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusConflict {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusConflict)
	}

	req, err = http.NewRequest("DELETE", "/latest", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(URLHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusConflict {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusConflict)
	}

	testBody = []byte(`{"baseCurrency":"EUR","targetCurrency":"NOK"}`)
	testIo = bytes.NewReader(testBody)

	req, err = http.NewRequest("POST", "/latest", testIo)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(URLHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	req, err = http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(URLHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}

	testBody = []byte(`{"webhookURL":"deleteMe","baseCurrency":"EUR","targetCurrency":"NOK","minTriggerValue":8,"maxTriggerValue":10}`)
	testIo = bytes.NewReader(testBody)

	req, err = http.NewRequest("POST", "/", testIo)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(URLHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if !strings.Contains(rr.Body.String(), "ID") {
		t.Error("request body did not contain expected: got %s want %s",
			rr.Body.String(), "ID")
	}

	db := GetADB()
	db.DeleteByURL("deleteMe")
	_, got := db.Get("deleteMe")
	if got == true {
		t.Error("Failed to remove test object!")
	}

	req, err = http.NewRequest("POST", "/59fb3890a35109d282bd046f", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(URLHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusConflict {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusConflict)
	}

	req, err = http.NewRequest("DELETE", "/djs8675686sdhadjlkhashj", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(URLHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNotFound {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}

	req, err = http.NewRequest("GET", "/59fb3890a35109d282bd046f", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(URLHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if !strings.Contains(rr.Body.String(), `"webhookURL":"keke","baseCurrency":"EUR","targetCurrency":"NOK","minTriggerValue":8,"maxTriggerValue":10`) {
		t.Error("request body did not contain expected: got %s want %s",
			rr.Body.String(), `"webhookURL":"keke","baseCurrency":"EUR","targetCurrency":"NOK","minTriggerValue":8,"maxTriggerValue":10`)
	}
}
