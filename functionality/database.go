package functionality

//Sources:
// https://github.com/marni/imt2681_cloud/blob/master/mongodb

import (
	"encoding/json"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
	"time"
)

//MongoInterface ...
// All of the functionality for db
type MongoInterface interface {
	Init()
	Add(c CurrencyLoad) error
	Count() int
	Get(HookURL string) (CurrencyLoad, bool)
	GetByID(id string) (CurrencyLoad, bool)
	Exist(HookURL string) int
	GetLocalFixer(date string) (interface{}, bool)
	Delete(id string) bool
	DeleteByURL(id string) bool
	DailyCheckHooks()
	EvalDailyCheckHooks()
	AddDailyFix()
}

//MongoDBInfo ...
// Data for the db is contained in this struct
type MongoDBInfo struct {
	MongoURL               string
	DatabaseName           string
	CurrencyHookCollection string
	RatesCollection        string
}

//Init ...
// Initializes the database
func (db *MongoDBInfo) Init() {
	session, err := mgo.Dial(db.MongoURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	index := mgo.Index{
		Key:        []string{"Id"},
		Unique:     true,
		DropDups:   false,
		Background: true,
		Sparse:     true,
	}

	err = session.DB(db.DatabaseName).C(db.CurrencyHookCollection).EnsureIndex(index)
	if err != nil {
		panic(err)
	}
}

//Add ...
func (db *MongoDBInfo) Add(c CurrencyLoad) error {

	session, err := mgo.Dial(db.MongoURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	err = session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Insert(c)
	if err != nil {
		return err
	}

	return nil
}

//Count ...
func (db *MongoDBInfo) Count() int {
	session, err := mgo.Dial(db.MongoURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	// handle to "db"
	count, err := session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Count()
	if err != nil {
		log.Printf("error in Count(): %v", err.Error())
		return -1
	}

	return count
}

//Exist ...
func (db *MongoDBInfo) Exist(HookURL string) int {
	session, err := mgo.Dial(db.MongoURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	count, err := session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Find(bson.M{"webhookurl": HookURL}).Count()
	if err != nil {
		log.Printf("error in Count(): %v", err.Error())
		count = -1
	}

	return count
}

//Get...
//Get by url
//@Return: return one currencyData
func (db *MongoDBInfo) Get(HookURL string) (CurrencyLoad, bool) {
	session, err := mgo.Dial(db.MongoURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	currencyData := CurrencyLoad{}
	gotData := true

	err = session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Find(bson.M{"webhookurl": HookURL}).One(&currencyData)
	if err != nil {
		gotData = false
	}

	return currencyData, gotData
}

//GetById ...
func (db *MongoDBInfo) GetByID(id string) (CurrencyLoad, bool) {
	session, err := mgo.Dial(db.MongoURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	currencyData := CurrencyLoad{}
	gotData := true

	err = session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(&currencyData)
	if err != nil {
		gotData = false
	}

	return currencyData, gotData
}

func (db *MongoDBInfo) GetLocalFixer(date string) (interface{}, bool) {
	session, err := mgo.Dial(db.MongoURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	var ratesData interface{}
	gotData := true

	err = session.DB(db.DatabaseName).C(db.RatesCollection).Find(bson.M{"date": date}).One(&ratesData)
	if err != nil {
		gotData = false
	}

	return ratesData, gotData
}

func (db *MongoDBInfo) Delete(id string) bool {
	session, err := mgo.Dial(db.MongoURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	err = session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Remove(bson.M{"_id": bson.ObjectIdHex(id)})
	if err != nil {
		return false
	}
	return true
}

func (db *MongoDBInfo) DeleteByURL(URL string) bool {
	session, err := mgo.Dial(db.MongoURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	err = session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Remove(bson.M{"webhookurl": URL})
	if err != nil {
		return false
	}
	return true

}

func (db *MongoDBInfo) DailyCheckHooks() {

	session, err := mgo.Dial(db.MongoURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	aDate := time.Now().Local()
	stringTodayDate := aDate.Format("2006-01-02")

	var fixerInterface interface{}
	rawFixer := RawFixer{}

	err = session.DB(db.DatabaseName).C(db.RatesCollection).Find(bson.M{"date": stringTodayDate}).One(&fixerInterface)
	if err != nil {
		log.Printf("Failed to count db! \n Error: %v", err.Error())
		return
	}

	dayRateBody, err := json.Marshal(fixerInterface)
	if err != nil {
		log.Printf("Failed to convert internal data")
		return
	}
	err = json.Unmarshal(dayRateBody, &rawFixer)
	if err != nil {
		log.Printf("Failed to convert internal data")
		return
	}

	hook := CurrencyLoad{}

	dbSize, err := session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Count()
	if err != nil {
		log.Printf("Failed to count db! \n Error: %v", err.Error())
		return
	}

	if dbSize > 0 {
		for i := 1; i <= dbSize; i++ {
			err = session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Find(nil).Skip(dbSize - i).One(&hook)
			if err != nil {
				log.Printf("Failed retrieve data! \n Error: %v", err.Error())
				return
			}

			if hook.MaxTriggerValue < rawFixer.LocalRate[hook.TargetCurrency] ||
				hook.MinTriggerValue > rawFixer.LocalRate[hook.TargetCurrency] {

				hookContent := GetHookContent(hook, rawFixer)
				WebHookEntry(hookContent, hook.WebHookURL)

			}

		}
	}

}

func (db *MongoDBInfo) EvalDailyCheckHooks() {

	session, err := mgo.Dial(db.MongoURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	aDate := time.Now().Local()
	stringTodayDate := aDate.Format("2006-01-02")

	var fixerInterface interface{}
	rawFixer := RawFixer{}

	err = session.DB(db.DatabaseName).C(db.RatesCollection).Find(bson.M{"date": stringTodayDate}).One(&fixerInterface)
	if err != nil {
		log.Printf("Failed to count db! \n Error: %v", err.Error())
		return
	}

	dayRateBody, err := json.Marshal(fixerInterface)
	if err != nil {
		log.Printf("Failed to convert internal data")
		return
	}
	err = json.Unmarshal(dayRateBody, &rawFixer)
	if err != nil {
		log.Printf("Failed to convert internal data")
		return
	}

	hook := CurrencyLoad{}

	dbSize, err := session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Count()
	if err != nil {
		log.Printf("Failed to count db! \n Error: %v", err.Error())
		return
	}

	if dbSize > 0 {
		for i := 1; i <= dbSize; i++ {
			err = session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Find(nil).Skip(dbSize - i).One(&hook)
			if err != nil {
				log.Printf("Failed retrieve data! \n Error: %v", err.Error())
				return
			}

			hookContent := GetHookContent(hook, rawFixer)
			WebHookEntry(hookContent, hook.WebHookURL)
			//if failed

		}
	}

}

func (db *MongoDBInfo) AddDailyFix() {
	myClient := http.Client{
		Timeout: time.Second * 2,
	}

	rates, err := GetRates(&myClient, "EUR")
	if err != nil {
		log.Printf("Could not get daily fix! \n Error: %v", err.Error())
	}

	session, err := mgo.Dial(db.MongoURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	err = session.DB(db.DatabaseName).C(db.RatesCollection).Insert(rates)
	if err != nil {
		log.Printf("Could not save daily fix! \n Error: %v", err.Error())
	}
}
