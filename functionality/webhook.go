package functionality

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

func DailyRoutine() {
	db := GetADB()
	db.Init()
	db.AddDailyFix()
	db.DailyCheckHooks()
}

type WebhookInfo struct {
	Content string `json:"content"`
}

func WebHookEntry(what string, URL string) bool {
	if strings.Contains(URL, ".com") {
		info := WebhookInfo{}
		info.Content = what + "\n"
		raw, _ := json.Marshal(info)
		resp, err := http.Post(URL, "application/json", bytes.NewBuffer(raw))
		if err != nil {
			log.Println(err)
			log.Println(ioutil.ReadAll(resp.Body))
			return false
		}
		return true
	} else {
		return false
	}

}

func webhookMain() {

	//println("Heroku timer test at: " + time.Now().String())
	//delay := time.Second

	db := GetADB()
	db.Init()
	db.DailyCheckHooks()

	DailyRoutine()
	//time.Sleep(delay)

}
